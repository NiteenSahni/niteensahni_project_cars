let problem6 = (array)=>
{     
    if(Array.isArray(array)==false)
    {
        return []
    }
      if(array.length==0)
      {
        return array;
      }
    let BMWandAudi =[]
    for(let i = 0; i<array.length; i++)
    {
       if(array[i].car_make == "Audi" || array[i].car_make == "BMW")
       {
        BMWandAudi.push(array[i])
       }
    }
    return BMWandAudi;
}
module.exports = problem6;