let problem4 = (array)=>
{
    if(Array.isArray(array)==false)
    {
        return []
    }
    if(array.length==0)
    {
        return array;
    }
    let carYears = []
    for(let i = 0; i<array.length; i++)
    {
        carYears[i]=array[i].car_year
    }
    return carYears
}
module.exports = problem4;