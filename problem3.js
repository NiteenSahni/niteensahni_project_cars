let problem3 = (array)=>

{  
    if(Array.isArray(array)==false)
    {
        return []
    }
    if(array.length==0)
    {
        return array;
    }
      let modelArray = []
    for(let i = 0 ;i<array.length; i++)
    {
     modelArray[i]=array[i].car_model
    }
    modelArray.sort()
    return modelArray
}

module.exports = problem3;