let problem5 =(inventory, array)=>

{   
    if(Array.isArray(array)==false)
    {
        return []
    }
    if(array.length==0)
    {
        return array;
    }
    let olderThan2000 =[]
    for(let i= 0; i<inventory.length; i++)
    {
       if(inventory[i].car_year<2000)
       {
        olderThan2000.push(inventory[i])
       }
    }
    
  
    let oldCars =[]
    for(let i= 0; i<array.length; i++)
    {
       if(array[i]<2000)
       {
        oldCars.push(array[i])
       }
    }
    return (oldCars.length,olderThan2000)
}
module.exports = problem5;